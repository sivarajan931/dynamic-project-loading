from RestrictedPython import safe_builtins, compile_restricted, PrintCollector
from RestrictedPython.Eval import default_guarded_getitem

from RestrictedPython import safe_builtins, compile_restricted

_SAFE_MODULES = frozenset(("math", "os"))


def _safe_import(name, *args, **kwargs):
    if name not in _SAFE_MODULES:
        raise Exception(f"Don't you even think about {name!r}")
    return __import__(name, *args, **kwargs)


def execute_user_code(user_code, user_func, *args, **kwargs):
    my_globals = {
        "__builtins__": {
            **safe_builtins,
            "__import__": _safe_import,
            "_print_": PrintCollector},
    }

    try:
        byte_code = compile_restricted(
            user_code, filename="<user_code>", mode="exec")
    except SyntaxError:
        # syntax error in the sandboxed code
        raise

    try:
        exec(byte_code, my_globals)
        return my_globals[user_func](*args, **kwargs)
    except BaseException:
        # runtime error (probably) in the sandboxed code
        raise


i_example = """
import math

def foo():
    return 7

def myceil(x):
    return math.ceil(x)+foo()

def testos():
    import os
    os.system("echo I am a dangerous code.")
    print(os.getcwd())
"""
print(execute_user_code(i_example, "testos"))
