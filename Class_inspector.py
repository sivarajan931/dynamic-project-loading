import os
import importlib.util
import sys
import inspect


# Function to load a module from a file path
def load_module(module_name, file_path):
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    return module


# Function to find and instantiate all classes within a module, and print their attributes
def instantiate_and_inspect_module_classes(module):
    # Filter all classes defined in the module
    classes = [member for name, member in inspect.getmembers(module, inspect.isclass) if
               member.__module__ == module.__name__]
    instances = []
    for cls in classes:
        print(f'Class: {cls.__name__}')
        instance = cls()  # Instantiate the class
        instances.append(instance)

        # Get and print all attributes and methods of the class
        attributes = inspect.getmembers(instance, lambda a: not (inspect.isroutine(a)))
        methods = inspect.getmembers(instance, inspect.ismethod)
        print('  Attributes:')
        for attr in attributes:
            if not (attr[0].startswith('__') and attr[0].endswith('__')):  # Filter out built-in properties
                print(f'    {attr[0]}: {attr[1]}')
        print('  Methods:')
        for method in methods:
            if not (method[0].startswith('__') and method[0].endswith('__')):  # Filter out built-in methods
                print(f'    {method[0]}')
        print('--------------------------')
    return instances


extensions_dir = 'extensions'
files = [f for f in os.listdir(extensions_dir) if f.endswith('.py')]

for file in files:
    module_name = file[:-3]  # Remove the .py extension to get the module name
    file_path = os.path.join(extensions_dir, file)
    module = load_module(module_name, file_path)

    # Instantiate all classes found in the module and inspect their attributes
    instantiate_and_inspect_module_classes(module)
