# Inside extension1.py


from extensions.my_extensions import m

def s():
    return "Hello World!"

class ext1(m):

    def __init__(self, *args):

        super().__init__()
        self.something = "why are you looking at this??"
        self.others = ['f', 'g', 'h']
        self.arguments = args
        self.h = dict()


    def run(self):
        self.others.append(self.something)
        self.others.append(s())

        return self.others

    def eat(self):
        return "EATING"

    def print_args(self):
        return self.arguments
