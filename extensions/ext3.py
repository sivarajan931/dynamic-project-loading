# Ext3 is designed to be error prone.
from extensions.my_extensions import m


class ext3(m):

    def __init__(self, *args):
        self.arguments = args

    def run(self):
        print("Extension 1 running")
        # Attempt to use a restricted built-in function
        try:
            with open('somefile.txt', 'r') as f:
                print(f.read())
        except Exception as e:
            print("Failed to open file:", str(e))

        # Attempt to import a module which is not allowed
        try:
            import os
            print("OS module imported successfully")
        except ImportError as e:
            print("ImportError:", str(e))

    def eat(self):
        print("Eating")

    def print_args(self):
        return self.arguments
