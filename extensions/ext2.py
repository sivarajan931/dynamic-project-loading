# Inside extension1.py
from extensions.my_extensions import m


class ext2(m):
    def __init__(self, *args):
        self.arguments = args

    def run(self):
        print("Extension 2 running")
        d = {'a': 'b'}
        for k, v in d.items():
            pass

    def print_args(self):
        return self.arguments
