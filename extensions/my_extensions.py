class m:
    def __init__(self):
        self.lscomp = [i for i in range(100) if i % 2 == 0]
    def execute_action(self, action):
        f = getattr(self, action)
        if f is not None:
            return f()

    def superclass_action(self):
        print(" This is a superclass action")
        return "Super class action works1!!"
