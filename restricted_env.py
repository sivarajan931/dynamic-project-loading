import ast
import builtins

import RestrictedPython
from RestrictedPython import safe_builtins, compile_restricted, PrintCollector
from RestrictedPython.Eval import default_guarded_getiter

from RestrictedPython.Guards import guarded_iter_unpack_sequence

import os

from RestrictedPython.transformer import INSPECT_ATTRIBUTES, copy_locations, astStr

from Tansformer import ModifiedNTransformer

_SAFE_MODULES = frozenset(
    ("math", "extensions.my_extensions", "my_extensions", "__future__"))  # List of modules deemed safe to import.


# Custom import function to restrict imports to safe modules.
def _safe_import(module_name, *args, **kwargs):
    if module_name not in _SAFE_MODULES:
        raise Exception(f"Import of {module_name!r} is not allowed as extension is untrusted")
    return __import__(module_name, *args, **kwargs)


# Dummy function to mimic write functionality in a safe manner.
def _write_(x):
    return x


class ExtensionExecutor(object):
    my_globals = {
        "__builtins__": {
            **safe_builtins,  # Include Python's safe builtins.
            "__import__": _safe_import,  # Use the custom safe import function.
            "_print_": PrintCollector,  # Collect print output instead of printing it.
            '_getattr_': getattr,  # Use a safer getattr to limit attribute access.
            '_setattr_': setattr,  # Restrict attribute setting to safe objects.
            '_write_': _write_,  # Safe write function.
            'dict': dict,
            'list': list,
            'set': set,


        },
        '__metaclass__': type,  # Set the metaclass to type (default).

        '__name__': 'extensions',  # Set the module name.

        '_iter_unpack_sequence_': guarded_iter_unpack_sequence,
        '_getiter_': default_guarded_getiter,
        'object':object,
        'super':super
    }


    def get_class(self, user_code, class_name):
        try:
            byte_code = compile_restricted(user_code, filename="<user_code>", mode="exec",policy=ModifiedNTransformer)
            exec(byte_code, ExtensionExecutor.my_globals)
            return ExtensionExecutor.my_globals[class_name]

        except SyntaxError as e:
            raise e


# Example usage: Load and execute code from .py files in the 'extensions' directory.


all_extensions = [i for i in os.listdir('extensions') if i.endswith('.py') and i != 'my_extensions.py']
all_extensions = sorted(all_extensions)

print(all_extensions)
for i in all_extensions:
    if i.endswith('.py'):
        print('___________________________________________________________________')
        with open(f"./extensions/{i}") as f:
            s = f.read()

        # Attempt to execute each file's contents using the restricted environment.
        try:
            e = ExtensionExecutor()
            cls = e.get_class(s, i[:-3])
            print(type(cls))
            print(cls)
            obj = cls("a", 'b', 1, 2, 3, 4)
            print(obj.print_args())
            print(obj.run())

        except Exception as e:
            print(f"Extension {i}: " + str(e))

        print('___________________________________________________________________')
