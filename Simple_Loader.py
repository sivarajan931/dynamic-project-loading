import os
import importlib.util
import sys
import inspect


# Function to load a module from a file path
def load_module(module_name, file_path):
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    return module


# Function to find and instantiate all classes within a module
def instantiate_module_classes(module):
    # Use inspect to filter all classes defined in the module
    # This will not list imported classes
    classes = [member for name, member in inspect.getmembers(module, inspect.isclass) if
               member.__module__ == module.__name__]
    instances = []
    for cls in classes:
        instances.append(cls())  # Instantiate the class
        print(f'Instantiated {cls.__name__}')
    return instances


extensions_dir = 'extensions'
files = [f for f in os.listdir(extensions_dir) if f.endswith('.py')]

for file in files:
    module_name = file[:-3]  # Remove the .py extension to get the module name
    file_path = os.path.join(extensions_dir, file)
    module = load_module(module_name, file_path)

    # Instantiate all classes found in the module
    instances = instantiate_module_classes(module)
    for instance in instances:
        if hasattr(instance, 'run'):  # Check if the instance has a 'run' method
            instance.run()
