import builtins

from RestrictedPython import safe_builtins

safer_builtins = safe_builtins
y = dir(builtins)

f = open('builtin_function.txt', 'w+')
for i in y:
    f.write(i + "\n")

f = open('safe_builtins.txt', 'w+')
for j in safer_builtins:
    f.write(j + "\n")

z = [i for i in y if i not in safer_builtins]


for k in z:
    print(k)
