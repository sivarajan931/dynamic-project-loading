from RestrictedPython import safe_builtins, compile_restricted, PrintCollector

from RestrictedPython.Guards import guarded_setattr, safer_getattr

import os

_SAFE_MODULES = frozenset(("math", "extensions.my_extensions","my_extensions"))  # List of modules deemed safe to import.


# Custom import function to restrict imports to safe modules.
def _safe_import(module_name, *args, **kwargs):
    if module_name not in _SAFE_MODULES:
        raise Exception(f"Import of {module_name!r} is not allowed as extension is untrusted")
    return __import__(module_name, *args, **kwargs)


# Dummy function to mimic write functionality in a safe manner.
def _write_(x):
    return x


# Executes user-provided Python code in a restricted environment.
def execute_user_code(user_code, user_func, class_name='f', arg=None, kw=None, *args, **kwargs):
    # Define a restricted global environment for executing user code.

    my_globals = {
        "__builtins__": {
            **safe_builtins,  # Include Python's safe builtins.
            "__import__": _safe_import,  # Use the custom safe import function.
            "_print_": PrintCollector,  # Collect print output instead of printing it.
            '_getattr_': safer_getattr,  # Use a safer getattr to limit attribute access.
            '_setattr_': guarded_setattr,  # Restrict attribute setting to safe objects.
            '_write_': _write_,  # Safe write function.
        },
        '__metaclass__': type,  # Set the metaclass to type (default).
        '__name__': 'extensions',  # Set the module name.
    }

    # Compile the user code in a restricted manner to prevent security risks.
    try:
        byte_code = compile_restricted(user_code, filename="<user_code>", mode="exec")
    except SyntaxError as e:
        raise e

    # Execute the compiled bytecode in the defined restricted environment.
    try:
        exec(byte_code, my_globals)
        # print(my_globals)  # Debug: print the global environment after execution.

        print(f"Class {class_name}")  # Debug: print the class name being used.
        # print(dir(my_globals[class_name]))  # Debug: print attributes of the class.

        # Instantiate the user-defined class and call the specified method.
        obj = my_globals[class_name](*args, **kwargs)

        method = getattr(obj, user_func, None)
        if method:

            return method()
        else:
            raise Exception(f'Method {user_func} not found')
    except BaseException as e:
        raise e


# Example usage: Load and execute code from .py files in the 'extensions' directory.


all_extensions = [i for i in os.listdir('extensions') if i.endswith('.py') and i != 'my_extensions.py']
all_extensions = sorted(all_extensions)
print(all_extensions)
for i in all_extensions:
    if i.endswith('.py'):
        print('___________________________________________________________________')
        with open(f"./extensions/{i}") as f:
            s = f.read()

        # Attempt to execute each file's contents using the restricted environment.
        try:
            print(execute_user_code(s, "run", i[:-3]))
            # Attempt to execute superclass action
            print(execute_user_code(s, "superclass_action", i[:-3]))

        except Exception as e:
            print(f"Extension {i}: " + str(e))
        print('___________________________________________________________________')